package com.sicc.edu;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class EduForGradleController {

	@RequestMapping("/")
	public String SpringDockerTest() {
		return "{'"
				+ "========================================="
				+ "\n\r"
				+ "		[:: 8th In-House Education For Sicc ::]		"
				+ "\n\r"
				+ "========================================="
				+ "'}";
	}
}